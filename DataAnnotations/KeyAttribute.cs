﻿/* Copyright Jarle Moe 2014
 * 
 * http://www.apache.org/licenses/LICENSE-2.0.html
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuddlefluff.Data.DataAnnotations
{
    public enum GeneratedMethod
    {
        Database,
        Custom
    }
    [AttributeUsage(AttributeTargets.Property)]
    public class KeyAttribute : Attribute
    {
        public GeneratedMethod Type { get; set; }
        public string Name { get; set; }    // Probably not necessary, stick to ColumnAttribute
    }
}
