﻿/* Copyright Jarle Moe 2014
 * 
 * http://www.apache.org/licenses/LICENSE-2.0.html
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuddlefluff.Data.DataAnnotations
{
    /// <summary>
    /// Marks a property as code-only and should not be reflected in the database
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CodeOnlyAttribute : Attribute
    {
    }
}
