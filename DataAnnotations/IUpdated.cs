﻿/* Copyright Jarle Moe 2014
 * 
 * http://www.apache.org/licenses/LICENSE-2.0.html
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuddlefluff.Data.DataAnnotations
{
    /// <summary>
    /// Marks the entity has having an Updated property which will automatically be populated by a repository
    /// </summary>
    public interface IUpdated
    {
        /// <summary>
        /// Gets or sets when this entity was last updated
        /// </summary>
        DateTime Updated { get; set; }
    }
}
