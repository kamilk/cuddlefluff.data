﻿/* Copyright Jarle Moe 2014
 * 
 * http://www.apache.org/licenses/LICENSE-2.0.html
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuddlefluff.Data.Cache
{
    public class NoCache : ICache
    {
        public object Get(string key)
        {
            return null;
        }

        public void Insert(string key, object value)
        {
        }

        public void Insert(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {   
        }

        public void Remove(string key)
        {
        }

        public void Dispose()
        {

        }
    }
}
